This is just a few suggestions/ideas on software that could be handy to use for admin, marketing and development.
Could be an idea to spend a day signing up for and integrating a few packages.
There's way to integrate most of these with Slack/Asana too.


## Admin


__CRM:__ Hubspot

An inbound marketing and sales software that helps companies attract visitors, 
convert leads, and close customers.
Keeps track of customer communications, set sales tasks, keep profiles of customers, 
save email templates, schedule emails for optiomal sending times, track email opens, onsite live chat etc.

An account comes with courses etc in using it effectively.

There's a few different levels, the free tier gets you the CRM and a free version 
of the sales software, then the marketing parts fairly pricey.

[This](https://www.hubspot.com/what-is-hubspot) probably explains the gist of it the best.

Package specific details:

- [CRM](https://www.hubspot.com/products/crm)
- [Sales](https://www.hubspot.com/products/sales)
- [Marketing](https://www.hubspot.com/products/get-started)

Would be perfect if we were getting someone in for marketing/social media, think Flynn said something about Natasha?

----

__Live Chat:__ Intercom.io

A much cheaper live chat alternative to hubspot, it integrates with facebook messenger which could be handy.

----

__Finances:__ Zipbooks

Zipbooks handles budgets, payroll, invoices, automatic invoice payment reminders,
quotes, and all sorts of other finance stuff. Been using it myself for ages and it's been handy out. 

You can also send digital invoices that can accept credit/debit card payments there and then.

An alternative is Freshbooks which integrates with Asana.



## Development



__Web Workflow:__

Very much just a rough idea but this tends to be what me and some others I know use and its working well for me so far! Did a good bit of research into it and this seems to be the best balance between flexibility, speed of development, and range of support/documentation options I could come up with.

Keeps as few moving parts as possible, and using the same stuff should cut down on any debugging time etc. For brochure sites with super quick turnarounds could get HTML themes and throw advanced dcustom fields on whatever small areas they need.

By taking the week to learn these, maybe trying them out on a simple brochure site to start with, the pipeline for every site from then on would be the same. Every site would get built faster than the last, and by the time we had a few sites done we'd be like wizards with the amount of snippets saved etc. The tools are also really powerful with a lot of possibilities, so easy sites would be done really quickly and bigger/more complex ones will have a foundation that can easily be extended an built on.

For a long term gameplan I think it's definitley the way to go.


1. Bootstrap with [Gulp](https://www.quora.com/What-exactly-does-Gulp-js-or-GruntJS-do-and-why-should-I-learn-them) tasks for compiling [SCSS](https://www.webdesignerdepot.com/2014/08/5-reasons-you-should-be-using-sass-today/), JS etc.
2. Wordpress with [Underscores](https://underscores.me/), [Advanced Custom Fields](https://www.advancedcustomfields.com/) and [Gravity Forms.](www.gravityforms.com/)
   Local developmnent on [MAMP](https://www.mamp.info/en/) or [WAMP](http://www.wampserver.com/en/) for local databases and servers, with [Gitlab](https://gitlab.com/) for version control. 
   Gitlab is basically Github but has unlimited free private repos.
3. For E-Commerce: WooCommerce and the Yith Themes premium plugin subscription 
   (I've a subscription with 30 site licences, theres about 150 plugins on the
   site and they're pretty damn useful) then official WooCommerce plugins for larger functional changes.

__Web App workflow__

This would be for any much more complicated web apps that could also run as the base for mobile and desktop apps. I''m far from an expert here never got much further than some simple enough Angularjs and VueJS projects but from any devs I know and what I've been reading this seems like the way to go, but its all just a suggestion!

1.[React](https://reactjs.org/) facebooks javascript library for the front end.

2.[Laravel](https://laravel.com/) PHP library for backends. Has its own extension for writing RESTful APIs so other apps and services can access the information cross platform. [Some benefits here](https://belitsoft.com/laravel-development-services/10-benefits-using-laravel-php-framework) but basically has a lot of busy work done for you already.
    
__Desktop App__

[Electron](https://electron.atom.io/) basically wraps webapps in a chrome wrapper for desktop. [Good post about it](https://medium.com/@Agro/developing-desktop-applications-with-electron-and-react-40d117d97564)
 
__Mobile Apps__

[React Native](https://facebook.github.io/react-native/)

----

__Official WooCommerce Plugins:__

These plugins have pretty high upfront costs but when you buy multi-site licences the price drops a lot. For the most part, a 25 site licence is cheaper than 2 single site licences. Keeping to official plugins will mean the best tech support and integration, since all these plugins get updated alongside woocommerce there should never be conflicts, and they are designed with integration in mind.

I'd recomend waiting until each one is needed by a client site to purchase, and work the 25 site licences into the pricing of each initial project. 
That way margins on sites using these plugins afterwards will be higher.

Not an essential list at all but a good one to give the idea of how powerful/big we can go with sites using pretty much built in functionality.

[Bookings](https://woocommerce.com/products/woocommerce-bookings/) a good booking app. 

[Subscriptions](https://woocommerce.com/products/woocommerce-subscriptions/) recurring payments integration.

[Multi Vendor Marketplace](https://woocommerce.com/products/product-vendors/) good one for if anyone wants to set up an etsy/tripadvisor/anything like that sort of site.

[Composite products](https://docs.woocommerce.com/document/composite-products/composite-products-use-case-kit-builder/) plugin for multi step, configurable products.

[Product Add ons](https://woocommerce.com/products/product-add-ons/?_ga=2.105072387.1249343623.1505472674-1498904006.1505472674) allows for product customization and integrates with Gravity Forms

[Product Add-Ons Gravity forms](https://woocommerce.com/products/gravity-forms-add-ons/) Powerful integration with Gravity Forms.

[Memberships](https://woocommerce.com/products/woocommerce-memberships/) would integrate great with multi vendor, subscriptions etc for some really big projects. Premium memberships leading to discounts on e-commerce stores, 'silver/gold memberships' etc etc.


----


__Yith Themes Plugins:__

 Like I mentioned, I already have a 30 site licence for all of the plugins on [this](https://yithemes.com/) site. Heres just a couple of examples of ones I've used.
 
[Abandoned Cart](https://yithemes.com/themes/plugins/yith-woocommerce-recover-abandoned-cart/) first of a series of plugins for improving customer loyalty. Between this and the powerful coupon plugin you have a lot of optiosn for turning once off customers into loyal customers.

[Coupon Email System](https://yithemes.com/themes/plugins/yith-woocommerce-coupon-email-system/) send a coupon with a discount for the birthday of one of your customers, or create coupons for customers that don’t make a purchase since many days, just to revive their interest in what’s new in your e-commerce site. Loads of options.

[Delivery Tracking](https://yithemes.com/themes/plugins/yith-woocommerce-order-tracking/)

[AJAX Search](https://yithemes.com/themes/plugins/yith-woocommerce-ajax-search/) Easy to integrate product search

[Frquently bought together](http://plugins.yithemes.com/yith-woocommerce-frequently-bought-together) using this and linked products can do a lot for cross selling and increasing e-commerce site revenue.

[Social Login](https://yithemes.com/themes/plugins/yith-woocommerce-social-login/) let customers login through facebook etc.
